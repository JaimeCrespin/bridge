import UIKit

protocol webApp {
    func introduce()
}
class Blog: webApp  {
    func introduce() {
        print("Hi! Im a blog")
    }
}
class Store: webApp  {
    func introduce() {
        print("Hi! Im a store")
    }
}
protocol coloredWeb {
    var webapp: webApp  { get set }
    func introduce()
}

class lightWeb: coloredWeb {
    var webapp: webApp
    init(webapp: webApp) {
        self.webapp = webapp
    }
    func introduce() {
        webapp.introduce()
        print("and Im have a light theme.")
    }
}

let blog = Blog()
let ligthBlog = lightWeb(webapp: blog)
ligthBlog.introduce()

let store = Store()
let ligthStore = lightWeb(webapp: store)
ligthStore.introduce()
